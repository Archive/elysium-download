# Translation into the walloon language.
#
# Si vos voloz donner on cp di spale pol ratournaedje di Gnome (ou des
# tes libes programes) sicrijhoz mu a l' adresse emile
# <srtxg@chanae.alphanet.ch>; nos avans co br�int di l' ovraedje a f�
#
# Copyright (C) 2002 Free Software Foundation, Inc.
# Pablo Saratxaga <srtxg@chanae.alphanet.ch> 2002
#
msgid ""
msgstr ""
"Project-Id-Version: elysium-download 1.3.0\n"
"POT-Creation-Date: 2002-06-25 21:21-0400\n"
"PO-Revision-Date: 2002-05-31 20:15MET\n"
"Last-Translator: Pablo Saratxaga <pablo@mandrakesoft.com>\n"
"Language-Team: Walon <linux-wa@chanae.alphanet.ch>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: capplet/capplet.c:204
#, fuzzy
msgid "Download Manager Properties"
msgstr "Manaedjeu d' aberwetaedjes Elysium"

#: capplet/capplet.c:216
msgid "Location"
msgstr "Eplaeçmint"

#: capplet/capplet.c:220
msgid "Download Location:"
msgstr "Eplaeçmint po-z aberweter:"

#: capplet/capplet.c:225
msgid "Download Location"
msgstr "Eplaeçmint po-z aberweter"

#: capplet/capplet.c:240
msgid "Always use default directory"
msgstr "Tofer eployî li prémetou ridant"

#: capplet/capplet.c:252
msgid "Always match mime type of file"
msgstr "Tofer loukî kel sôre MIME do fitchî corespondexhe"

#: capplet/capplet.c:264
msgid "Always ask for a directory"
msgstr "Tofer dimander après on ridant"

#: capplet/capplet.c:278
msgid "General"
msgstr "Djenerå"

#: capplet/capplet.c:282
msgid "Maximum Concurrent Downloads:"
msgstr "Nombe macsimom d' aberwetaedjes e minme tins:"

#: capplet/capplet.c:296
msgid "Display File Complete Dialog"
msgstr "Håyner on purnea di dialogue cwand l' fitchî est fini d' aberweter"

#: src/GNOME_Elysium_Download_Shell.server.in.h:1
msgid "The Elysium Download shell."
msgstr "Li shell d' aberwetaedje d' Elysium"

#: src/elysium-display.c:25 src/elysium-display.c:223
msgid "Elysium Download Manager"
msgstr "Manaedjeu d' aberwetaedjes Elysium"

#: src/elysium-display.c:28
#, fuzzy
msgid "A Download Manager for the GNOME Desktop."
msgstr "On manaedjeu d' aberwetaedjes pol sicribanne Gnome"

#: src/elysium-display.c:48
msgid "New"
msgstr "Novea"

#: src/elysium-display.c:48
msgid "Add a new file to the queue"
msgstr "Radjouter on novea fitchî el caweye"

#: src/elysium-display.c:55
msgid "Stop"
msgstr "Arester"

#: src/elysium-display.c:55
msgid "Cancel downloading the selected item"
msgstr "Arester l' aberwetaedje pol cayet tchoezi"

#: src/elysium-display.c:62 src/elysium-display.c:81
msgid "Quit"
msgstr "Moussî foû"

#: src/elysium-display.c:62 src/elysium-display.c:81
msgid "Exit the Elysium Download Manager"
msgstr "Moussî foû do manaedjeu d' aberwetaedjes Elysium"

#: src/elysium-display.c:73 src/elysium-display.c:285 src/elysium-docklet.c:27
msgid "Add New URI"
msgstr "Radjouter ene novele hårdêye"

#: src/elysium-display.c:73 src/elysium-docklet.c:27
msgid "Add a new URI to download"
msgstr "Radjouter ene novele hårdêye (URI) po-z aberweter"

#: src/elysium-display.c:92 src/elysium-docklet.c:35
msgid "Preferences"
msgstr "Preferinces"

#: src/elysium-display.c:92 src/elysium-docklet.c:35
msgid "Change settings"
msgstr "Candjî les apontiaedjes"

#: src/elysium-display.c:103 src/elysium-docklet.c:42
msgid "A_bout..."
msgstr "Å _dfait..."

#: src/elysium-display.c:103 src/elysium-docklet.c:42
msgid "Info about Elysium Download Manager"
msgstr "Informåcion å dfait do manaedjeu d' aberwetaedjes Elysium"

#: src/elysium-display.c:112
msgid "_File"
msgstr "_Fitchî"

#: src/elysium-display.c:113
msgid "_Edit"
msgstr "_Candjî"

#: src/elysium-display.c:114
msgid "_Help"
msgstr "_Aidance"

#: src/elysium-display.c:218
msgid "Source"
msgstr "Sourdant"

#: src/elysium-display.c:219
msgid "Status"
msgstr "Etat"

#: src/elysium-display.c:291
msgid "URI:"
msgstr "Hårdêye:"

#: src/elysium-display.c:316
msgid ""
"The URI you entered was invalid.\n"
"Please try again."
msgstr ""
"Li hårdeye (URI) ki vos avoz tapé n' est nén valåve.\n"
"Sayîs co ene feye s' i vs plait."

#: src/elysium-display.c:339
msgid "The capplet could not be found."
msgstr "L' aplikete n' a nén stî trovêye."

#: src/elysium-docklet.c:163
msgid "No downloads in progress"
msgstr "Nou aberwetaedje pol moumint"

#: src/elysium-vfs.c:112
#, c-format
msgid "Download of %s completed."
msgstr "Aberwetaedje di %s fini."

#: src/elysium-vfs.c:116
#, fuzzy
msgid "Download Complete"
msgstr "Aberwetaedje di %s fini."

#: src/elysium-vfs.c:308
msgid "File "
msgstr "Li fitchî "

#: src/elysium-vfs.c:310
msgid " exists. Overwrite?"
msgstr " egzisteye dedja. El sipotchî?"

#: src/elysium-vfs.c:462
msgid "Save File As..."
msgstr "Schaper eyet rlomer l' fitchî..."

#~ msgid "Use the Status Dock"
#~ msgstr "Eployî li panea d' etat"

#~ msgid "Downloading %s"
#~ msgstr "Aberwetaedje di %s"
