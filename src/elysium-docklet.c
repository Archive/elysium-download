#include "elysium-download.h"
#include <gdk/gdkx.h>

enum {
    TARGET_REORDER = 0,
    TARGET_URL,
    TARGET_URILIST
};

static GtkTargetEntry target_table[] = {
  { "gtk-clist-drag-reorder", 0, TARGET_REORDER },
  { "x-url/http", 0, TARGET_URL },
  { "x-url/ftp", 0, TARGET_URL },
#ifdef ENABLE_VFS_SSL
  { "x-url/https", 0, TARGET_URL },
#endif
  { "_NETSCAPE_URL", 0, TARGET_URL },
  { "x-url/smb", 0, TARGET_URL },
  { "text/plain", 0, TARGET_URILIST }
};

static GnomeUIInfo dock_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("Add New URI"), N_("Add a new URI to download"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    N_("Preferences"), N_("Change settings"),
    ed_run_capplet, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
    FALSE, FALSE, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    N_("A_bout..."), N_("Info about Elysium Download Manager"),
    ed_about, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
    FALSE, FALSE, NULL
  },    
  GNOMEUIINFO_END
};

static void ed_add_drag_url (GtkWidget *widget, GdkDragContext *context,
			     gint x, gint y, GtkSelectionData *selection_data,
			     guint info, guint time, EDWindow * window) {

  g_return_if_fail (widget != NULL);
  if (strstr (selection_data->data, "://") == NULL)
    return;
  switch (info) {
  case TARGET_URL: {
    gchar * moo = NULL;
    EncompassURI * uri = NULL;
    moo = g_strndup (selection_data->data, strlen (selection_data->data)
		     - strlen (strchr (selection_data->data, '\n')));
    uri = encompass_uri_new (moo);
    ed_vfs_queue_uri (encompass_uri_dup (uri, ENCOMPASS_URI_DUP_ALL),
		      window);
    encompass_uri_destroy (uri);
    g_free (moo);
    break;
  }
  case TARGET_URILIST: {
    EncompassURI * uri = NULL;
    gint i;
    gchar ** uri_list = g_strsplit (selection_data->data, "\n", -1);
    for (i = 0; uri_list && uri_list[i] && *uri_list[i]; i++) {
      uri = encompass_uri_new (uri_list[i]);
      ed_vfs_queue_uri (encompass_uri_dup (uri, ENCOMPASS_URI_DUP_ALL),
			window);
      encompass_uri_destroy (uri);
    }
    g_strfreev (uri_list);
    break;
  }
  default:
    break;
  }
}

void ed_setup_drag_list (EDWindow * window) {
  gtk_drag_dest_unset (window->dlist);
  gtk_drag_dest_set (window->dlist, GTK_DEST_DEFAULT_ALL, target_table,
		     sizeof (target_table) / sizeof (target_table[0]),
		     GDK_ACTION_COPY | GDK_ACTION_MOVE);
  gtk_signal_connect (GTK_OBJECT (window->dlist), "drag_data_received",
		      GTK_SIGNAL_FUNC (ed_add_drag_url), window);
}

void ed_display_docklet (GtkWidget * widget, EDWindow * window) {
  gchar *status_pixfile;
  GtkWidget * menu_pop;
  GtkWidget * ebox;

  window->tooltips = gtk_tooltips_new ();

  window->docklet = egg_tray_icon_new ("Elysium Download Status");

  ebox = gtk_event_box_new ();
  gtk_container_add (GTK_CONTAINER (window->docklet), ebox);
  gtk_widget_show (ebox);

  status_pixfile = elysium_pixmap_file("elysium-download/status-idle.png");
  window->dock_icon = gnome_pixmap_new_from_file_at_size (status_pixfile,
							  16, 16);

  gtk_container_add(GTK_CONTAINER (ebox), window->dock_icon);
  gtk_widget_show(window->dock_icon);
  if (status_pixfile) {
    g_free (status_pixfile);
  }

  gtk_signal_connect (GTK_OBJECT (ebox), "button_press_event",
		      GTK_SIGNAL_FUNC (ed_main_hide_or_show), window);

  gtk_tooltips_set_tip (window->tooltips, window->docklet,
			_("No downloads in progress"), NULL);

  menu_pop = gnome_popup_menu_new (dock_menu);
  gnome_popup_menu_attach (menu_pop, ebox, NULL);
  gtk_signal_connect (GTK_OBJECT (dock_menu[0].widget), "activate",
		      GTK_SIGNAL_FUNC (ed_new_uri_dialog), window);

  /* Drag and Drop stuff */
  /* Status Dock Icon */
  gtk_drag_dest_unset (ebox);
  gtk_drag_dest_set (ebox, GTK_DEST_DEFAULT_ALL, target_table,
		     sizeof (target_table) / sizeof (target_table[0]),
		     GDK_ACTION_COPY | GDK_ACTION_MOVE);
  gtk_signal_connect (GTK_OBJECT (ebox), "drag_data_received",
		      GTK_SIGNAL_FUNC (ed_add_drag_url), window);
  /* Status List */
  ed_setup_drag_list (window);

  /* Only show the docklet if user wants it */
  gtk_widget_show (GTK_WIDGET (window->docklet));
}
