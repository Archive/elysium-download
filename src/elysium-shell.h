#ifndef _ELYSIUM_SHELL_H_
#define _ELYSIUM_SHELL_H_

typedef struct _EShellPrivate EShellPrivate;

typedef struct {
  BonoboObject parent;

  EShellPrivate * priv;
} EShell;

typedef struct {
  BonoboObjectClass parent_class;

  POA_GNOME_Elysium_Download_Shell__epv epv;
} EShellClass;

#define E_TYPE_SHELL (elysium_shell_get_type())
#define E_SHELL(obj) (GTK_CHECK_CAST ((obj), E_TYPE_SHELL, EShell))
#define E_SHELL_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), E_TYPE_SHELL, EShellClass))
#define E_IS_SHELL(obj)     (GTK_CHECK_TYPE ((obj), E_TYPE_SHELL))
#define E_IS_SHELL_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((obj), E_TYPE_SHELL))

GtkType elysium_shell_get_type (void);
gboolean elysium_shell_construct (EShell * eshell,
				    const gchar * iid);
EShell * elysium_shell_new (void);

#endif
