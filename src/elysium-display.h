#ifndef _ELYSIUM_DISPLAY_H_
#define _ELYSIUM_DISPLAY_H_

struct _EDWindow {
  GtkWidget * docklet;
  GtkWidget * dock_icon;

  GtkTooltips * tooltips;

  GtkWidget * window;
  GtkWidget * dlist;

  GList * edqueue_list;
  GtkWidget * stop_item;

  GConfClient * client;
  EDPrefs * prefs;
};

typedef struct {
  GtkWidget * dialog;
} EDDialog;

void ed_about (GtkWidget * widget, gpointer data);
void ed_display_dialog (GnomeVFSXferProgressInfo * info, EDWindow * window,
			EDownloadItem * ditem);
gboolean ed_main_hide_or_show (GtkWidget * widget, GdkEventButton * event, 
			       EDWindow * window);
void ed_display_create_main_window (GtkWidget * widget, EDWindow * ewindow);
void ed_new_uri_dialog (GtkWidget * widget, EDWindow * window);
void ed_run_capplet (GtkWidget * widget, EDWindow * window);

#endif
