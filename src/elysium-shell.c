#include "elysium-download.h"

#define PARENT_TYPE bonobo_object_get_type ()

struct _EShellPrivate {
  gchar * iid;
};

static void impl_Shell_save_uri_as (PortableServer_Servant servant,
				    const CORBA_char * uri,
				    CORBA_Environment * ev) {
  EShell * eshell;
  EShellPrivate * priv;
  EncompassURI * tmpuri;
  gboolean use_dir = FALSE, use_app = FALSE;

  eshell = E_SHELL (bonobo_object_from_servant (servant));
  priv = eshell->priv;

  tmpuri = encompass_uri_new ((gchar *)uri);

  if (edwindow->prefs->always_use_app) {
    edwindow->prefs->always_use_app = FALSE;
    use_app = TRUE;
  } else if (edwindow->prefs->always_use_dir) {
    edwindow->prefs->always_use_dir = FALSE;
    use_dir = TRUE;
  }
  if (!edwindow->prefs->always_ask_dir)
    edwindow->prefs->always_ask_dir = TRUE;

  ed_vfs_queue_uri (tmpuri, edwindow);

  if (use_dir) {
    use_dir = FALSE;
    edwindow->prefs->always_ask_dir = FALSE;
    edwindow->prefs->always_use_dir = TRUE;
  } else if (use_app) {
    use_app = FALSE;
    edwindow->prefs->always_use_app = TRUE;
    edwindow->prefs->always_ask_dir = FALSE;
  }

  encompass_uri_destroy (tmpuri);
}

static void impl_Shell_handleURI (PortableServer_Servant servant,
				  const CORBA_char * uri,
				  CORBA_Environment * ev) {
  EShell * eshell;
  EShellPrivate * priv;
  EncompassURI * tmpuri;

  eshell = E_SHELL (bonobo_object_from_servant (servant));
  priv = eshell->priv;

  tmpuri = encompass_uri_new ((gchar *)uri);

  ed_vfs_queue_uri (tmpuri, edwindow);

  encompass_uri_destroy (tmpuri);
}

static void impl_Shell_toggleWindow (PortableServer_Servant servant,
				     CORBA_Environment * ev) {
  if (GTK_WIDGET_VISIBLE (edwindow->window)) {
    gtk_widget_hide (edwindow->window);
  } else {
    gtk_widget_show (edwindow->window);
  }
}

BONOBO_CLASS_BOILERPLATE_FULL (EShell, elysium_shell,
			       GNOME_Elysium_Download_Shell, BonoboObject,
			       BONOBO_OBJECT_TYPE)

static void destroy (GtkObject * object) {
  EShell * eshell;
  EShellPrivate * priv;

  eshell = E_SHELL (object);
  priv = eshell->priv;
  if (priv->iid != NULL) {
    bonobo_activation_active_server_unregister (priv->iid,
						bonobo_object_corba_objref (BONOBO_OBJECT (eshell)));
  }
  g_free (priv);
}

static void elysium_shell_class_init (EShellClass * klass) {
  BonoboObjectClass * object_class;
  POA_GNOME_Elysium_Download_Shell__epv * epv;

  parent_class = gtk_type_class (PARENT_TYPE);

  object_class = BONOBO_OBJECT_CLASS (klass);

  epv = &klass->epv;
  epv->save_uri_as = impl_Shell_save_uri_as;
  epv->handleURI = impl_Shell_handleURI;
  epv->toggleWindow = impl_Shell_toggleWindow;
}

static void elysium_shell_instance_init (EShell * eshell) {
  EShellPrivate *priv;

  priv = g_new0 (EShellPrivate, 1);
  priv->iid = NULL;
  eshell->priv = priv;
}

gboolean elysium_shell_construct (EShell * eshell,
				    const gchar * iid) {
  EShellPrivate * priv;
  CORBA_Object corba_object;

  g_return_val_if_fail (eshell != NULL, FALSE);

  corba_object = bonobo_object_corba_objref (BONOBO_OBJECT (eshell));
  if (bonobo_activation_active_server_register (iid, corba_object) !=
      Bonobo_ACTIVATION_REG_SUCCESS) {
    return FALSE;
  }

  while (gtk_events_pending ()) {
    gtk_main_iteration ();
  }

  priv = eshell->priv;

  priv->iid = g_strdup (iid);

  return TRUE;
}

EShell * elysium_shell_new (void) {
  EShell * new;
  EShellPrivate * priv;

  new = g_object_new (elysium_shell_get_type (), NULL);

  if (!elysium_shell_construct (new, "OAFIID:GNOME_Elysium_Download_Shell")) {
    bonobo_object_unref (BONOBO_OBJECT (new));
    return NULL;
  }

  priv = new->priv;

  return new;
}
