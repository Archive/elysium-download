/*
  Copyright 2001-2002 (C) Elysium GNU/Linux
*/

#include "elysium-download.h"

static EShell * shell = NULL;

static gint elysium_idle_cb (void * data) {
  GSList * uri_list = NULL;
  GNOME_Elysium_Download_Shell corba_shell;
  CORBA_Environment ev;
  gboolean restored;

  CORBA_exception_init (&ev);

  uri_list = (GSList *) data;

  shell = elysium_shell_new ();
  if (shell == NULL) {
    corba_shell =
      bonobo_activation_activate_from_id ("OAFIID:GNOME_Elysium_Download_Shell",
					  0, NULL, &ev);
    if (ev._major != CORBA_NO_EXCEPTION || corba_shell == CORBA_OBJECT_NIL) {
      CORBA_exception_free (&ev);
      bonobo_main_quit ();
      return FALSE;
    }
    restored = FALSE;
  } else {
    if (uri_list == NULL) {
      restored = TRUE;
    } else {
      restored = FALSE;
    }
    corba_shell = bonobo_object_corba_objref (BONOBO_OBJECT (shell));
    corba_shell = CORBA_Object_duplicate (corba_shell, &ev);
    Bonobo_Unknown_ref (corba_shell, &ev);
    ed_display_create_main_window (NULL, edwindow);
  }

  if (!restored && uri_list == NULL) {
  } else {
    GSList * p;

    for (p = uri_list; p != NULL; p = p->next) {
      gchar * uri;

      uri = (gchar *) p->data;
      
			if (!g_strcasecmp (uri, "toggle-window")) {
	GNOME_Elysium_Download_Shell_toggleWindow (corba_shell, &ev);
      } else if (strstr (uri, ":/")) {
	GNOME_Elysium_Download_Shell_handleURI (corba_shell, uri, &ev);
      } else {
	gchar * tempurl;

	/* Assume file: if first char is /,
	   Assume ftp: if first part of uri is "ftp."
	   Otherwise, assume http:
	 */
	if (uri[0] == '/') {
	  tempurl = g_strdup_printf ("file://%s", uri);
	  GNOME_Elysium_Download_Shell_handleURI (corba_shell,
						  g_strdup (tempurl),
						  &ev);
	  g_free (tempurl);
	} else if (!g_strncasecmp (uri, "ftp.", strlen ("ftp."))) {
	  tempurl = g_strdup_printf ("ftp://%s", uri);
	  GNOME_Elysium_Download_Shell_handleURI (corba_shell,
						  g_strdup (tempurl),
						  &ev);
	  g_free (tempurl);
	} else {
	  tempurl = g_strdup_printf ("http://%s", uri);
	  GNOME_Elysium_Download_Shell_handleURI (corba_shell,
						  g_strdup (tempurl),
						  &ev);
	  g_free (tempurl);
	}
      }
    }
    g_slist_free (uri_list);
  }

  CORBA_exception_free (&ev);

  if (shell == NULL) {
    bonobo_main_quit ();
  }
  return FALSE;
}

gint main(gint argc, gchar *argv[]) {

  CORBA_Environment ev;
  CORBA_ORB orb;
  GError * gconf_error = NULL;
  GnomeProgram * edproggie;
  GSList * uri_list = NULL;
  poptContext ctx;
  const gchar ** args;
  GValue context = { 0 };

  CORBA_exception_init (&ev);

  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  edproggie = gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
				  argc, argv, GNOME_PARAM_POPT_TABLE,
				  NULL, NULL);

  g_object_get_property (G_OBJECT (edproggie), GNOME_PARAM_POPT_CONTEXT,
			 g_value_init (&context, G_TYPE_POINTER));
  ctx = g_value_get_pointer (&context);

  if (!bonobo_activation_is_initialized ()) {
    orb = bonobo_activation_init (argc, argv);
  } else {
    orb = bonobo_activation_orb_get ();
  }

  gdk_rgb_init ();
  gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
  gtk_widget_set_default_visual (gdk_rgb_get_visual ());

  if (!gconf_init (argc, argv, &gconf_error)) {
    g_assert (gconf_error != NULL);
    g_error ("GConf init failed:\n  %s", gconf_error->message);
    return FALSE;
  }

  gnome_vfs_init ();

  edwindow = g_new0 (EDWindow, 1);
  
  uri_list = NULL;
  edwindow->edqueue_list = NULL;

  args = poptGetArgs (ctx);
  if (args != NULL) {
    const gchar ** p;

    for (p = args; *p != NULL; p++) {
      uri_list = g_slist_prepend (uri_list, (gchar *) *p);
    }
  }

  poptFreeContext (ctx);

  g_idle_add (elysium_idle_cb, uri_list);
  bonobo_main ();

  return 0;
}
