#include "elysium-download.h"
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime.h>

typedef struct {
  EDWindow * window;
  EncompassURI * uri;
  gchar * local_uri;
} SaveDialogCrap;

typedef struct {
  EDWindow * window;
  EDownloadItem * ditem;
} EDownloadCrap;

static gint downloading = 0;
static gint num_files = 0;
static gint queue_size = 0;

static void ed_vfs_download (EDWindow * window, GList * source_files_list,
			     GList * dest_files_list,  EDownloadItem * ditem);

static void ed_vfs_check_queue (EDWindow * window) {
  if (queue_size > 0 && num_files >= 0 &&
      num_files < window->prefs->max_downloads) {
    GList * source_file_list = NULL;
    GList * save_file_list = NULL;
    gchar * tmpfoo;
    EDownloadItem * ditem;

    ditem = g_list_nth_data (window->edqueue_list, 0);

    source_file_list = g_list_append (source_file_list,
				      gnome_vfs_uri_dup (ditem->source_uri));
    tmpfoo = gnome_vfs_uri_to_string (ditem->source_uri,
				      GNOME_VFS_URI_HIDE_PASSWORD);
						  
    save_file_list = g_list_append (save_file_list,
				    gnome_vfs_uri_dup (ditem->local_uri));

    window->edqueue_list = g_list_remove (window->edqueue_list, ditem);

    queue_size--;
    ed_vfs_download (window, source_file_list, save_file_list, ditem);
    gtk_tooltips_set_tip (window->tooltips, window->docklet,
			  g_strdup_printf (_("Downloading %s"), 
					   g_basename (tmpfoo)), NULL);
    num_files++;
    g_free (tmpfoo);
  } else {
    if (GTK_CLIST (window->dlist)->rows == 0) {
      gchar * icon;
      
      icon = elysium_pixmap_file ("elysium-download/status-idle.png");
      gnome_pixmap_load_file_at_size (GNOME_PIXMAP (window->dock_icon),
				      icon, 16, 16);
      gtk_tooltips_set_tip (window->tooltips, window->docklet,
			    _("No downloads in progress"), NULL);
      g_free (icon);

      downloading = 0;
    }
  }
}

void ed_vfs_download_cancel (EDWindow * window, EDownloadItem * ditem) {
  EDownloadItem * dnitem;
  gint i;
  gchar * tmpfile;
  EncompassURI * uri;

  gnome_vfs_async_cancel (ditem->handle);
  tmpfile = gnome_vfs_uri_to_string (ditem->local_uri,
				     GNOME_VFS_URI_HIDE_PASSWORD);
  uri = encompass_uri_new (g_strdup (tmpfile));
  g_free (tmpfile);
  if (g_file_exists (uri->path)) {
    remove (uri->path);
  }
  encompass_uri_destroy (uri);
  gtk_clist_remove (GTK_CLIST (window->dlist), ditem->row);

  g_free (ditem->local_uri);
  g_free (ditem->source_uri);
  g_free (ditem);

  gtk_clist_freeze (GTK_CLIST (window->dlist));
  for (i = 0; i < GTK_CLIST (window->dlist)->rows; i++) {
    dnitem = NULL;
    dnitem = gtk_clist_get_row_data (GTK_CLIST (window->dlist), i);
    dnitem->row = i;
  }
  gtk_clist_thaw (GTK_CLIST (window->dlist));
  gtk_widget_queue_resize (window->dlist);

  num_files--;
  ed_vfs_check_queue (window);
}

static void ed_vfs_download_complete (GnomeVFSXferProgressInfo * info,
				      EDownloadCrap * smack) {
  EDownloadItem * ditem;
  GtkWidget * mbox;
  gchar * message;
  gchar * text;
  gint i;

  if (smack->window->prefs->donedialog) {
    message = g_strdup_printf (_("Download of %s completed."),
			       g_basename (info->source_name));
    mbox = gnome_message_box_new (message, GNOME_MESSAGE_BOX_INFO,
				  GNOME_STOCK_BUTTON_OK, NULL);
    gtk_window_set_title (GTK_WINDOW (mbox), _("Download Complete"));
    gtk_widget_show (mbox);
  }
  gtk_clist_remove (GTK_CLIST (smack->window->dlist), smack->ditem->row);
  gtk_clist_freeze (GTK_CLIST (smack->window->dlist));
  for (i = 0; i < GTK_CLIST (smack->window->dlist)->rows; i++) {
    ditem = NULL;
    ditem = gtk_clist_get_row_data (GTK_CLIST (smack->window->dlist), i);
    ditem->row = i;
  }
  gtk_clist_thaw (GTK_CLIST (smack->window->dlist));
  gtk_widget_queue_resize (smack->window->dlist);

  g_free (smack->ditem->source_uri);
  g_free (smack->ditem->local_uri);
  g_free (smack->ditem);

  num_files--;
  ed_vfs_check_queue (smack->window);
}

static void ed_vfs_progress (GnomeVFSAsyncHandle * handle,
			     GnomeVFSXferProgressInfo * info,
			     EDownloadCrap * smack) {
  switch (info->phase) {
  case GNOME_VFS_XFER_PHASE_COPYING: {
    ed_display_dialog (info, smack->window, smack->ditem);
    switch (info->vfs_status) {
    case GNOME_VFS_OK: {
      break;
    }  
    default: {
      gnome_vfs_async_cancel (handle);
      ed_vfs_check_queue (smack->window);
      break;
    }
    }
    break;
  }
  case GNOME_VFS_XFER_PHASE_CLOSETARGET: {
    if (info != NULL) {
      ed_vfs_download_complete (info, smack);
    }
    break;
  }
  case GNOME_VFS_XFER_PHASE_COMPLETED: {
    while (gtk_events_pending ()) {
      gtk_main_iteration ();
    }
    ed_vfs_check_queue (smack->window);
    break;
  }
  default: {
    break;
  }
  }
}

static void ed_vfs_download (EDWindow * window, GList * source_files_list,
			     GList * dest_files_list,  EDownloadItem * ditem) {
  gchar * icon;
  EDownloadCrap * smack = g_new0 (EDownloadCrap, 1);

  smack->window = window;
  smack->ditem = ditem;

  icon = elysium_pixmap_file ("elysium-download/status-downloading.png");
  gnome_pixmap_load_file_at_size (GNOME_PIXMAP (window->dock_icon),
				  icon, 16, 16);
  g_free (icon);

  downloading = 1;
  gnome_vfs_async_xfer (&ditem->handle,
			source_files_list, dest_files_list,
			GNOME_VFS_XFER_DEFAULT,
			GNOME_VFS_XFER_ERROR_MODE_ABORT,
			GNOME_VFS_XFER_OVERWRITE_MODE_REPLACE, 0,
			(GnomeVFSAsyncXferProgressCallback) 
			ed_vfs_progress,
			smack,
			NULL, NULL);
}

static void ed_vfs_add_to_queue (GnomeVFSURI * source_uri,
				 GnomeVFSURI * local_uri,
				 EDWindow * window) {
  gchar * url[2] = { NULL, NULL };
  EDownloadItem * ditem = g_new0 (EDownloadItem, 1);

  ditem->source_uri = gnome_vfs_uri_dup (source_uri);
  ditem->local_uri = gnome_vfs_uri_dup (local_uri);

  url[0] = g_strdup (gnome_vfs_uri_to_string (gnome_vfs_uri_dup (source_uri),
					      GNOME_VFS_URI_HIDE_PASSWORD));
  url[1] = g_strdup ("0.00%");

  ditem->row = gtk_clist_append (GTK_CLIST (window->dlist), url);
  g_free (url[0]);
  g_free (url[1]);

  window->edqueue_list = g_list_append (window->edqueue_list, ditem);
  gtk_clist_set_row_data (GTK_CLIST (window->dlist), ditem->row, ditem);
  queue_size++;
  if ((!downloading && queue_size > 0 && num_files == 0) ||
      (queue_size > 0 && num_files >= 0 &&
       num_files < window->prefs->max_downloads)) {
    gchar * tmpfoo;
    GList * source_file_list = NULL;
    GList * save_file_list = NULL;

    source_file_list = g_list_append (source_file_list,
				      gnome_vfs_uri_dup (source_uri));
    save_file_list = g_list_append (save_file_list,
				    gnome_vfs_uri_dup (local_uri));

    window->edqueue_list = g_list_remove (window->edqueue_list, ditem);

    queue_size--;
    if (queue_size <= 0) {
      window->edqueue_list = NULL;
    }
    tmpfoo = gnome_vfs_uri_to_string (source_uri,
				      GNOME_VFS_URI_HIDE_PASSWORD);
    ed_vfs_download (window, source_file_list, save_file_list, ditem);
    gtk_tooltips_set_tip (window->tooltips, window->docklet,
			  g_strdup_printf (_("Downloading %s"), 
					   g_basename (tmpfoo)), NULL);
    num_files++;
    g_free (tmpfoo);
  }
}

static void ed_vfs_parse_directory (GnomeVFSAsyncHandle * handle,
				    GnomeVFSResult result,
				    GList * file_list,
				    guint file_entries,
				    SaveDialogCrap * crap) {
  GnomeVFSFileInfo * info;

  info = g_list_nth_data (file_list, 0);
  switch (result) {
  case GNOME_VFS_OK: {
    GnomeVFSURI * source;
    GnomeVFSURI * local;
    gchar * temp;

    if (!g_strncasecmp (info->mime_type, "x-directory/",
		       strlen ("x-directory/"))) {
      return;
    } else {
      temp = g_strconcat (g_strdup (encompass_uri_to_string (crap->uri)),
			  g_strdup (info->name), NULL);
      source = gnome_vfs_uri_new (g_strdup (temp));
      local = gnome_vfs_uri_new (g_strconcat (g_strdup (crap->local_uri),
					      g_strdup (info->name), NULL));
      ed_vfs_add_to_queue (source, local, crap->window);
    }
    break;
  }
  case GNOME_VFS_ERROR_EOF: {
    break;
  }
  default: {
    break;
  }
  }
  return;
}

static void ed_vfs_save_loc (GtkWidget * button,
			     SaveDialogCrap * crap) {
  GtkFileSelection * filesel;
  gchar * fname;
  gchar * remote_file;

  filesel = GTK_FILE_SELECTION (gtk_widget_get_toplevel (button));
  fname = g_strdup (gtk_file_selection_get_filename (filesel));
  
  if (fname == NULL || *fname == '\0') {
    g_free (fname);
    gtk_widget_destroy (GTK_WIDGET (filesel));
  } else {
    GnomeVFSURI * e_vfs_uri;
    GnomeVFSURI * e_loc_uri;

    if (g_file_exists (fname) && fname[strlen (fname) - 1] != '/') {
      GtkWidget * mbox;

      mbox = gnome_message_box_new (g_strconcat (_("File "),
						 g_strdup (fname),
						 _(" exists. Overwrite?"),
						 NULL),
				    GNOME_MESSAGE_BOX_WARNING,
				    GNOME_STOCK_BUTTON_YES,
				    GNOME_STOCK_BUTTON_NO,
				    NULL);
      switch (gnome_dialog_run_and_close (GNOME_DIALOG(mbox))) {
      case 0: {
	remove (g_strdup (fname));
	break;
      }
      default: {
	g_free (fname);
	return;
	break;
      }
      }
    } else if (!g_file_exists (fname) && fname[strlen (fname) - 1] == '/') {
      mkdir (fname, 0755);
    }
    remote_file = g_strdup (encompass_uri_to_string (crap->uri));
    e_vfs_uri = gnome_vfs_uri_new (g_strdup (remote_file));
    e_loc_uri = gnome_vfs_uri_new (gnome_vfs_get_uri_from_local_path (fname));

    crap->local_uri = gnome_vfs_uri_to_string (gnome_vfs_uri_dup (e_loc_uri),
					       GNOME_VFS_URI_HIDE_PASSWORD);
    
    if (remote_file[strlen (remote_file) - 1] == '/') {
      GnomeVFSAsyncHandle * handle;

      gnome_vfs_async_load_directory_uri (&handle,
					  gnome_vfs_uri_dup (e_vfs_uri),
					  GNOME_VFS_FILE_INFO_GET_MIME_TYPE,
					  1, 0,
					  (GnomeVFSAsyncDirectoryLoadCallback)
					  ed_vfs_parse_directory,
					  crap);
      g_free (remote_file);
    } else {
      ed_vfs_add_to_queue (gnome_vfs_uri_dup (e_vfs_uri),
			   gnome_vfs_uri_dup (e_loc_uri), crap->window);
    }

    g_free (e_vfs_uri);
    g_free (e_loc_uri);

    gtk_widget_destroy (GTK_WIDGET (filesel));
  }
  g_free (fname);
}

static void ed_vfs_cancel_save_loc (GtkWidget * button,
				    gpointer data) {
  GtkWidget * filesel;

  filesel = gtk_widget_get_toplevel (button);
  gtk_widget_destroy (filesel);
}

static void ed_vfs_download_default (SaveDialogCrap * crap,
				     EDWindow * window) {
  gchar * remote_file, * fname;
  GnomeVFSURI * e_vfs_uri, * e_loc_uri;

  remote_file = g_strdup (encompass_uri_to_string (crap->uri));

  if (remote_file[strlen (remote_file) - 1] == '/') {
    gchar * blah;

    blah = g_strdup (remote_file);
    blah[strlen (blah) - 1] = blah[strlen (blah)];
    fname = g_strconcat (window->prefs->downloads_dir,
			 g_strdup (strrchr (blah, '/')), "/", NULL);
    g_free (blah);
    if (!g_file_exists (fname)) {
      mkdir (fname, 0755);
    }
  } else {
    fname = g_strconcat (window->prefs->downloads_dir, "/",
			 g_basename (remote_file), NULL);
  }
  e_vfs_uri = gnome_vfs_uri_new (g_strdup (remote_file));
  e_loc_uri = gnome_vfs_uri_new (gnome_vfs_get_uri_from_local_path (fname));

  crap->local_uri = gnome_vfs_uri_to_string (gnome_vfs_uri_dup (e_loc_uri),
					     GNOME_VFS_URI_HIDE_PASSWORD);
  
  if (remote_file[strlen (remote_file) - 1] == '/') {
    GnomeVFSAsyncHandle * handle;

    gnome_vfs_async_load_directory_uri (&handle,
					gnome_vfs_uri_dup (e_vfs_uri),
					GNOME_VFS_FILE_INFO_GET_MIME_TYPE,
					1, 0,
					(GnomeVFSAsyncDirectoryLoadCallback)
					ed_vfs_parse_directory,
					crap);
    g_free (remote_file);
    g_free (fname);
  } else {
    ed_vfs_add_to_queue (gnome_vfs_uri_dup (e_vfs_uri),
			 gnome_vfs_uri_dup (e_loc_uri), crap->window);
  }

  g_free (e_vfs_uri);
  g_free (e_loc_uri);    
}

void ed_vfs_queue_uri (EncompassURI * uri, EDWindow * window) {
  GtkWidget * filesel;
  SaveDialogCrap * crap;

  crap = g_new (SaveDialogCrap, 1);

  crap->window = window;
  crap->uri = encompass_uri_dup (uri, ENCOMPASS_URI_DUP_NOREFERENCE);

  if (window->prefs->always_use_app) {
    gchar * dlpath = NULL, * fname, * remote_file;
    GnomeVFSURI * e_vfs_uri = NULL, * e_loc_uri = NULL;

    remote_file = g_strdup (encompass_uri_to_string (crap->uri));
    e_vfs_uri = gnome_vfs_uri_new (g_strdup (remote_file));

    dlpath = (gchar *) gnome_vfs_mime_get_value (gnome_vfs_mime_type_from_name
						 (uri->path), "download-dir");

    if (dlpath != NULL && *dlpath) {
      if (!g_file_exists (dlpath)) {
	mkdir (dlpath, 0755);
      }
      if (dlpath[strlen (dlpath) - 1] != '/') {
	fname = g_strconcat (dlpath, "/", g_basename (remote_file), NULL);
      } else {
	fname = g_strconcat (dlpath, g_basename (remote_file), NULL);
      }
      e_loc_uri =
	gnome_vfs_uri_new (gnome_vfs_get_uri_from_local_path (fname));
      crap->local_uri = gnome_vfs_uri_to_string (gnome_vfs_uri_dup (e_loc_uri),
						 GNOME_VFS_URI_HIDE_PASSWORD);

      ed_vfs_add_to_queue (e_vfs_uri, e_loc_uri, window);

      g_free (e_loc_uri);
      g_free (fname);
      g_free (dlpath);
    } else {
      ed_vfs_download_default (crap, window);
    }
    g_free (e_vfs_uri);
    g_free (remote_file);
  } else if (window->prefs->always_ask_dir) {
    filesel = gtk_file_selection_new (_("Save File As..."));
    gtk_window_set_wmclass (GTK_WINDOW (filesel), "filesel", "ED:FileSelect");
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (filesel),
				     g_basename(uri->path));
    g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
		      "clicked", G_CALLBACK (ed_vfs_save_loc), crap);
    g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
		      "clicked", G_CALLBACK (ed_vfs_cancel_save_loc), NULL);

    gtk_widget_show_all (filesel);
  } else {
    ed_vfs_download_default (crap, window);
  }
}
