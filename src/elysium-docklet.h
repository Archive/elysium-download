#ifndef _STATUS_ICON_H_
#define _STATUS_ICON_H_

#include "eggtrayicon.h"

void ed_setup_drag_list (EDWindow * window);
void ed_display_docklet (GtkWidget * widget, EDWindow * window);

#endif
