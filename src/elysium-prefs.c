#include "elysium-download.h"

static void ed_download_dir_changed (GConfClient *client, guint cnxn_id,
				     GConfEntry *entry, EDWindow * window) {
  window->prefs->downloads_dir = g_strdup (gconf_value_get_string (entry->value));
}

static void ed_download_type_changed (GConfClient *client, guint cnxn_id,
				      GConfEntry *entry, EDWindow * window) {
  window->prefs->dltype = gconf_value_get_int (entry->value);
  switch (window->prefs->dltype) {
  case 0:
    window->prefs->always_use_dir = TRUE;
    window->prefs->always_use_app = FALSE;
    window->prefs->always_ask_dir = FALSE;
    break;
  case 1:
    window->prefs->always_use_dir = FALSE;
    window->prefs->always_use_app = TRUE;
    window->prefs->always_ask_dir = FALSE;
    break;
  case 2:
    window->prefs->always_use_dir = FALSE;
    window->prefs->always_use_app = FALSE;
    window->prefs->always_ask_dir = TRUE;
    break;
  default:
    window->prefs->always_use_dir = TRUE;
    window->prefs->always_use_app = FALSE;
    window->prefs->always_ask_dir = FALSE;
    break;
  }
}

static void ed_max_downloads_changed (GConfClient *client, guint cnxn_id,
				      GConfEntry *entry, EDWindow * window) {
  window->prefs->max_downloads = gconf_value_get_int (entry->value);
}

static void ed_show_dialog_changed (GConfClient *client, guint cnxn_id,
				    GConfEntry *entry, EDWindow * window) {
  window->prefs->donedialog = gconf_value_get_bool (entry->value);
}

void ed_load_prefs (EDWindow * window) {
  gchar * temp;
  GError * gconf_error = NULL;

  window->prefs = g_new0 (EDPrefs, 1);
  window->client = gconf_client_get_default ();

  gconf_client_add_dir (window->client, "/apps/elysium-download",
			FALSE, &gconf_error);

  window->prefs->downloads_dir =
    gconf_client_get_string (window->client,
			     "/apps/elysium-download/location", NULL);

  window->prefs->dltype =
    gconf_client_get_int (window->client,
			  "/apps/elysium-download/download_type", NULL);
  gconf_client_set_int (window->client, "/apps/elysium-download/download_type",
			window->prefs->dltype, NULL);

  window->prefs->max_downloads =
    gconf_client_get_int (window->client,
			  "/apps/elysium-download/max_downloads", NULL);
  window->prefs->donedialog =
    gconf_client_get_bool (window->client,
			   "/apps/elysium-download/show_done", NULL);

  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/location",
			   (GConfClientNotifyFunc) ed_download_dir_changed,
			   window, NULL, NULL);
  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/download_type",
			   (GConfClientNotifyFunc) ed_download_type_changed,
			   window, NULL, NULL);
  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/max_downloads",
			   (GConfClientNotifyFunc) ed_max_downloads_changed,
			   window, NULL, NULL);
  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/show_done",
			   (GConfClientNotifyFunc) ed_show_dialog_changed,
			   window, NULL, NULL);

  if (window->prefs->downloads_dir == NULL || !*window->prefs->downloads_dir) {
    gconf_client_set_string (window->client,
			     "/apps/elysium-download/location",
			     g_strconcat (g_get_home_dir (), "/Downloads",
					  NULL), NULL);
    if (!g_file_exists (window->prefs->downloads_dir)) {
      mkdir (window->prefs->downloads_dir, 0755);
    }
  }
  gtk_widget_show (window->window);
}
