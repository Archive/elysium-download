#ifndef _ELYSIUM_DOWNLOAD_H_
#define _ELYSIUM_DOWNLOAD_H_

#include <gnome.h>
#include <bonobo-activation/bonobo-activation.h>
#include <bonobo.h>
#include <encompass-uri.h>
#include <elysium-file.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf-client.h>

#include <config.h>

#include "EDownload.h"

typedef struct _EDWindow EDWindow;
typedef struct _EDownloadItem EDownloadItem;

#include "elysium-shell.h"
#include "elysium-prefs.h"
#include "elysium-display.h"
#include "elysium-docklet.h"
#include "elysium-vfs.h"

EDWindow * edwindow;

#endif
