#include "elysium-download.h"
#include <libgnomeui/gnome-window-icon.h>

typedef struct {
  EDWindow * window;
  EDownloadItem * ditem;
} EDCancelCrap;

void ed_about (GtkWidget * widget, gpointer data) {
  static GtkWidget * about = NULL;
  const gchar * authors[] = {
    "Rodney Dawes <dobey@free.fr>",
    NULL
  };
  const gchar * documentors[] = {
    NULL
  };

  /* Please fill in the names of translators */
  const gchar * translator_credits = _("translator_credits");

  if (about != NULL) {
    gdk_window_raise (about->window);
    gdk_window_show (about->window);
    return;
  }

  about = gnome_about_new (_("Elysium Download Manager"),
			   VERSION,
			   "(C) 2001-2002 Elysium GNU/Linux",
			   _("A Download Manager for the GNOME Desktop."),
			   authors,
			   documentors,
			   strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
			   NULL);

  g_signal_connect (G_OBJECT (about), "destroy",
		    G_CALLBACK (gtk_widget_destroyed), &about);

  gtk_widget_show (about);
}

static void ed_main_quit (GtkWidget * widget, EDWindow * window) {
  gconf_client_remove_dir (window->client, "/apps/elysium-download", NULL);
  gnome_vfs_shutdown ();
  bonobo_main_quit ();
}

static GnomeUIInfo main_toolbar[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("New"), N_("Add a new file to the queue"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW,
    'N', GDK_CONTROL_MASK, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    N_("Stop"), N_("Cancel downloading the selected item"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_STOP,
    GDK_Escape, 0, NULL
  },
  {
    GNOME_APP_UI_ITEM,
    N_("Quit"), N_("Exit the Elysium Download Manager"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_QUIT,
    'Q', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo file_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("Add New URI"), N_("Add a new URI to download"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    N_("Quit"), N_("Exit the Elysium Download Manager"),
    NULL, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
    'Q', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("Preferences"), N_("Change settings"),
    ed_run_capplet, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  {
    GNOME_APP_UI_ITEM,
    N_("A_bout..."), N_("Info about Elysium Download Manager"),
    ed_about, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
    FALSE, FALSE, NULL
  },
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_SUBTREE(N_("_File"), file_menu),
  GNOMEUIINFO_SUBTREE(N_("_Edit"), edit_menu),
  GNOMEUIINFO_SUBTREE(N_("_Help"), help_menu),
  GNOMEUIINFO_END
};

void ed_display_dialog (GnomeVFSXferProgressInfo * info, EDWindow * window,
			EDownloadItem * ditem) {
  gchar * pcc;
  gchar * text;

  pcc = g_strdup_printf ("%5.2f%%",
			 ((gfloat) info->bytes_copied / 
			  (gfloat) info->file_size ) * 100);
  gtk_clist_set_text (GTK_CLIST (window->dlist), ditem->row, 1,
		      g_strdup (pcc));

  /*
    This is not good. Especially on a T1. 150 windows being created every
    second just isn't fun, or useful for about 1 minute after the download
    even finishes.
  */

  /*
  if (showdialog) {
    GtkWidget * mbox, * fromlabel, * tolabel, * pbar;

    mbox = gnome_dialog_new (g_strconcat (_("Downloading "),
					  g_basename (info->source_name),
					  NULL), NULL);
    fromlabel = gtk_label_new (g_strconcat (_("Downloading from "),
					    info->source_name, NULL));
    gtk_misc_set_alignment (GTK_MISC (fromlabel), 0, 0.5);
    gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (mbox)->vbox), fromlabel);
    gtk_widget_show (fromlabel);
    
    gtk_widget_show (mbox);
  }
  */
}

static void ed_main_hide (GtkWidget * widget, GdkEventAny * event,
			  EDWindow * window) {
  if (GTK_WIDGET_VISIBLE (window->window)) {
    gtk_widget_hide (window->window);
  } else {
    ed_main_quit (widget, window);
  }
}

gboolean ed_main_hide_or_show (GtkWidget * widget, GdkEventButton * event,
			       EDWindow * window) {
  if (event->button != 3) {
    if (GTK_WIDGET_VISIBLE (window->window)) {
      gtk_widget_hide (window->window);
    } else {
      gtk_widget_show (window->window);
    }
    return TRUE;
  } else {
    return FALSE;
  }
}

static void ed_create_toolbar_and_menus (GtkWidget * frame,
					 EDWindow * window) {
  gnome_app_create_toolbar (GNOME_APP (frame), main_toolbar);
  gnome_app_create_menus (GNOME_APP (frame), main_menu);

  g_signal_connect (G_OBJECT (file_menu[0].widget), "activate",
		    G_CALLBACK (ed_new_uri_dialog), window);

  g_signal_connect (G_OBJECT (main_toolbar[0].widget), "clicked",
		    G_CALLBACK (ed_new_uri_dialog), window);
}

static void ed_cancel_clicked (GtkWidget * widget, EDCancelCrap * spliff) {
  ed_vfs_download_cancel (spliff->window, spliff->ditem);
}

static gint gpq;

static void ed_display_row_selected (GtkWidget * widget, gint row,
				     gint column, GdkEvent * event,
				     EDWindow * window) {
  if (GTK_CLIST (window->dlist)->selection) {
    EDCancelCrap * spliff = g_new0 (EDCancelCrap, 1);
    spliff->window = window;
    spliff->ditem = gtk_clist_get_row_data (GTK_CLIST (window->dlist), row);

    gtk_widget_set_sensitive (window->stop_item, TRUE);
    gpq = g_signal_connect (G_OBJECT (window->stop_item), "clicked",
			    G_CALLBACK (ed_cancel_clicked), spliff);
  } else {
    gtk_signal_disconnect (GTK_OBJECT (window->stop_item), gpq);
    gtk_widget_set_sensitive (window->stop_item, FALSE);
  }
}

void ed_display_create_main_window (GtkWidget * widget, EDWindow * ewindow) {
  GtkWidget * scwin;
  gchar * icon;
  gchar * titles[] = {
    _("Source"),
    _("Status")
  };

  ewindow->window = gnome_app_new ("ElysiumDownload", 
				   _("Elysium Download Manager"));

  gtk_widget_realize (ewindow->window);
  gtk_widget_set_usize (ewindow->window, 400, 256);
  gtk_window_set_policy (GTK_WINDOW (ewindow->window), FALSE, TRUE, FALSE);

  icon = elysium_pixmap_file ("elysium-download.png");
  gnome_window_icon_set_from_file (GTK_WINDOW (ewindow->window), icon);
  if (icon) {
    g_free (icon);
  }

  ed_create_toolbar_and_menus (ewindow->window, ewindow);

  ewindow->stop_item = main_toolbar[1].widget;
  gtk_widget_set_sensitive (ewindow->stop_item, FALSE);

  scwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scwin),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);
  gnome_app_set_contents (GNOME_APP (ewindow->window), scwin);
  gtk_widget_show (scwin);

  ewindow->dlist = gtk_clist_new_with_titles (2, titles);
  gtk_container_add (GTK_CONTAINER (scwin), ewindow->dlist);
  gtk_widget_realize (ewindow->dlist);
  gtk_widget_show (ewindow->dlist);

  ed_setup_drag_list (ewindow);

  g_signal_connect (G_OBJECT (ewindow->dlist), "select_row",
		    G_CALLBACK (ed_display_row_selected), ewindow);
  g_signal_connect (G_OBJECT (ewindow->dlist), "unselect_row",
		    G_CALLBACK (ed_display_row_selected), ewindow);

  gtk_clist_set_shadow_type (GTK_CLIST (ewindow->dlist), GTK_SHADOW_IN);
  gtk_clist_set_column_width (GTK_CLIST (ewindow->dlist), 0, 340);
  gtk_clist_set_column_width (GTK_CLIST (ewindow->dlist), 1, 32);
  gtk_clist_set_column_justification (GTK_CLIST (ewindow->dlist), 1,
				      GTK_JUSTIFY_RIGHT);

  g_signal_connect (G_OBJECT (ewindow->window), "delete_event",
		    G_CALLBACK (ed_main_hide), ewindow);
  g_signal_connect (G_OBJECT (file_menu[2].widget), "activate",
		    G_CALLBACK (ed_main_quit), ewindow);
  g_signal_connect (G_OBJECT (main_toolbar[2].widget), "clicked",
		    G_CALLBACK (ed_main_quit), ewindow);

  ed_load_prefs (ewindow);

  if (!GTK_IS_WIDGET (ewindow->docklet)) {
    ed_display_docklet (NULL, ewindow);
  }
}

void ed_new_uri_dialog (GtkWidget * widget, EDWindow * window) {
  GtkWidget * dialog, * entry, * hbox, * label;
  gint result;

  dialog = gnome_dialog_new (_("Add New URI"), GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL, NULL);
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (hbox), 4);
  gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (dialog)->vbox), hbox);

  label = gtk_label_new (_("URI:"));
  gtk_container_add (GTK_CONTAINER (hbox), label);
  gtk_widget_show (label);

  entry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER (hbox), entry);
  gtk_widget_show (entry);

  gtk_widget_show (hbox);

  result = gnome_dialog_run (GNOME_DIALOG (dialog));
  switch (result) {
  case 0: {
    gchar * entered_uri;

    entered_uri = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));

    if (strstr (entered_uri, "://")) {
      EncompassURI * uri;

      uri = encompass_uri_new (g_strdup (entered_uri));
      ed_vfs_queue_uri (uri, window);
      g_free (entered_uri);
    } else {
      GtkWidget * mbox;
      mbox = gnome_message_box_new (_("The URI you entered was invalid.\n"
				      "Please try again."),
				    GNOME_MESSAGE_BOX_WARNING,
				    GNOME_STOCK_BUTTON_OK, NULL);
      gtk_widget_show (mbox);
    }
  }
  case 1: {
    gnome_dialog_close (GNOME_DIALOG (dialog));
  }
  }
}

void ed_run_capplet (GtkWidget * widget, EDWindow * window) {
  if (gnome_is_program_in_path ("elysium-download-capplet")) {
    gchar * argv[1];

    argv[0] = g_strdup ("elysium-download-capplet");
    gnome_execute_async (argv[0], 1, argv);
    g_free (argv[0]);
  } else {
    GtkWidget * msgbox;

    msgbox = gnome_message_box_new (_("The capplet could not be found."),
				  GNOME_MESSAGE_BOX_WARNING,
				  GNOME_STOCK_BUTTON_OK,
				  NULL);
    gtk_widget_show (msgbox);
  }
}
