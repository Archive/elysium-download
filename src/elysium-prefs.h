#ifndef _ELYSIUM_PREFS_H_
#define _ELYSIUM_PREFS_H_

typedef struct {
  gchar * downloads_dir;

  gint dltype;
  gboolean always_use_dir;
  gboolean always_use_app;
  gboolean always_ask_dir;

  gint max_downloads;
  gboolean donedialog;
  gboolean usedocklet;

} EDPrefs;

void ed_load_prefs (EDWindow * window);

#endif
