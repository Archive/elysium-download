#ifndef _ELYSIUM_VFS_H_
#define _ELYSIUM_VFS_H_

struct _EDownloadItem {
  GnomeVFSURI * source_uri;
  GnomeVFSURI * local_uri;
  gint row;
  GnomeVFSAsyncHandle * handle;
};

void ed_vfs_download_cancel (EDWindow * window, EDownloadItem * ditem);
void ed_vfs_queue_uri (EncompassURI * uri, EDWindow * window);

#endif
