/*
  Copyright 2001-2002 (C) Elysium GNU/Linux
*/

#include "capplet.h"

static void ed_download_dir_changed (GConfClient * client, guint cnxn_id,
				     GConfEntry * entry,
				     EDCappletWindow * window) {
  window->prefs->downloads_dir = g_strdup (gconf_value_get_string (entry->value));
}

static void ed_download_type_set_toggle (EDCappletWindow * window) {
  switch (window->prefs->dltype) {
  case 0:
    window->prefs->always_use_dir = TRUE;
    window->prefs->always_use_app = FALSE;
    window->prefs->always_ask_dir = FALSE;
    GTK_TOGGLE_BUTTON (window->noaskdl)->active = TRUE;
    GTK_TOGGLE_BUTTON (window->usehelper)->active = FALSE;
    GTK_TOGGLE_BUTTON (window->askdown)->active = FALSE;
    break;
  case 1:
    window->prefs->always_use_dir = FALSE;
    window->prefs->always_use_app = TRUE;
    window->prefs->always_ask_dir = FALSE;
    GTK_TOGGLE_BUTTON (window->noaskdl)->active = FALSE;
    GTK_TOGGLE_BUTTON (window->usehelper)->active = TRUE;
    GTK_TOGGLE_BUTTON (window->askdown)->active = FALSE;
    break;
  case 2:
    window->prefs->always_use_dir = FALSE;
    window->prefs->always_use_app = FALSE;
    window->prefs->always_ask_dir = TRUE;
    GTK_TOGGLE_BUTTON (window->noaskdl)->active = FALSE;
    GTK_TOGGLE_BUTTON (window->usehelper)->active = FALSE;
    GTK_TOGGLE_BUTTON (window->askdown)->active = TRUE;
    break;
  default:
    window->prefs->always_use_dir = TRUE;
    window->prefs->always_use_app = FALSE;
    window->prefs->always_ask_dir = FALSE;
    GTK_TOGGLE_BUTTON (window->noaskdl)->active = TRUE;
    GTK_TOGGLE_BUTTON (window->usehelper)->active = FALSE;
    GTK_TOGGLE_BUTTON (window->askdown)->active = FALSE;
    break;
  }
}

static void ed_download_type_changed (GConfClient * client, guint cnxn_id,
				      GConfEntry * entry,
				      EDCappletWindow * window) {
  window->prefs->dltype = gconf_value_get_int (entry->value);
  ed_download_type_set_toggle (window);
}

static void ed_max_downloads_changed (GConfClient * client, guint cnxn_id,
				      GConfEntry * entry,
				      EDCappletWindow * window) {
  window->prefs->max_downloads = gconf_value_get_int (entry->value);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (window->maxspin),
			     window->prefs->max_downloads);
}

static void ed_show_dialog_changed (GConfClient * client, guint cnxn_id,
				    GConfEntry * entry,
				    EDCappletWindow * window) {
  window->prefs->donedialog = gconf_value_get_bool (entry->value);
  GTK_TOGGLE_BUTTON (window->donedialog)->active = window->prefs->donedialog;
}

void ed_capplet_load_prefs (EDCappletWindow * window) {
  gchar * temp;
  GError * gconf_error = NULL;

  window->prefs = g_new0 (EDPrefs, 1);
  window->client = gconf_client_get_default ();

  gconf_client_add_dir (window->client, "/apps/elysium-download",
			FALSE, &gconf_error);

  window->prefs->downloads_dir =
    gconf_client_get_string (window->client,
			     "/apps/elysium-download/location", NULL);

  window->prefs->dltype =
    gconf_client_get_int (window->client,
			  "/apps/elysium-download/download_type", NULL);

  window->prefs->max_downloads =
    gconf_client_get_int (window->client,
			  "/apps/elysium-download/max_downloads", NULL);
  window->prefs->donedialog =
    gconf_client_get_bool (window->client,
			   "/apps/elysium-download/show_done", NULL);

  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/location",
			   (GConfClientNotifyFunc) ed_download_dir_changed,
			   window, NULL, NULL);
  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/download_type",
			   (GConfClientNotifyFunc) ed_download_type_changed,
			   window, NULL, NULL);
  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/max_downloads",
			   (GConfClientNotifyFunc) ed_max_downloads_changed,
			   window, NULL, NULL);
  gconf_client_notify_add (window->client,
			   "/apps/elysium-download/show_done",
			   (GConfClientNotifyFunc) ed_show_dialog_changed,
			   window, NULL, NULL);

  if (window->prefs->downloads_dir == NULL || !*window->prefs->downloads_dir) {
    gconf_client_set_string (window->client,
			     "/apps/elysium-download/location",
			     g_strconcat (g_get_home_dir (), "/Downloads",
					  NULL), NULL);
    if (!g_file_exists (window->prefs->downloads_dir)) {
      mkdir (window->prefs->downloads_dir, 0755);
    }
  }
}

static void ed_capplet_set_download_dir (GtkEntry * widget,
					 EDCappletWindow * window) {
  gchar * temp = NULL;
  temp = g_strdup (gtk_entry_get_text (widget));

  if (!g_file_exists (temp)) {
    mkdir (temp, 0755);
  }
  if (temp[strlen (temp) - 1] == '/') {
    window->prefs->downloads_dir = g_strndup (temp, strlen (temp) - 1);
  } else {
    window->prefs->downloads_dir = g_strdup (temp);
  }
  gconf_client_set_string (window->client,
			   "/apps/elysium-download/location",
			   window->prefs->downloads_dir, NULL);
  gtk_entry_set_text (widget, window->prefs->downloads_dir);
  g_free (temp);
}

static void ed_capplet_set_download_type (GtkWidget * widget,
					  EDCappletWindow * window) {
  window->prefs->always_use_dir =
    GTK_TOGGLE_BUTTON (window->noaskdl)->active;
  window->prefs->always_use_app =
    GTK_TOGGLE_BUTTON (window->usehelper)->active;
  window->prefs->always_ask_dir =
    GTK_TOGGLE_BUTTON (window->askdown)->active;

  if (window->prefs->always_ask_dir) {
    gconf_client_set_int (window->client,
			  "/apps/elysium-download/download_type", 2, NULL);
  } else if (window->prefs->always_use_app) {
    gconf_client_set_int (window->client,
			  "/apps/elysium-download/download_type", 1, NULL);
  } else {
    gconf_client_set_int (window->client,
			  "/apps/elysium-download/download_type", 0, NULL);
  }
}

static void ed_capplet_set_max_downloads (GtkWidget * widget,
					  EDCappletWindow * window) {
  window->prefs->max_downloads = 
    gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (window->maxspin));
  gconf_client_set_int (window->client,
			"/apps/elysium-download/max_downloads",
			window->prefs->max_downloads, NULL);
  
}

static void ed_capplet_set_show_dialog (GtkWidget * widget,
					EDCappletWindow * window) {
  window->prefs->donedialog =  GTK_TOGGLE_BUTTON (widget)->active;
  gconf_client_set_bool (window->client,
			 "/apps/elysium-download/show_done",
			 window->prefs->donedialog, NULL);
}

static void ed_capplet_close (GtkWidget * widget, EDCappletWindow * window) {
  gconf_client_remove_dir (window->client, "/apps/elysium-download", NULL);
  g_free (window->prefs->downloads_dir);

  gtk_main_quit ();
}

static void ed_capplet_close_clicked (GtkWidget * widget, gint button,
				      EDCappletWindow * window) {
  ed_capplet_close (widget, window);
}

static void ed_capplet_create_window (void) {
  EDCappletWindow * window;
  GtkWidget * page, * hbox, * label;
  GSList * ask_group = NULL;
  GtkObject * spinadj;

  window = g_new0 (EDCappletWindow, 1);

  window->window = gnome_dialog_new (_("Download Manager Properties"),
				     GTK_STOCK_CLOSE, NULL);

  ed_capplet_load_prefs (window);

  window->ntbook = gtk_notebook_new ();
  gtk_container_add (GTK_CONTAINER (GNOME_DIALOG (window->window)->vbox),
		     window->ntbook);
  gtk_widget_show (window->ntbook);

  page = gtk_vbox_new (FALSE, 0);
  gtk_notebook_append_page (GTK_NOTEBOOK (window->ntbook), page,
			    gtk_label_new (_("Location")));

  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (page), hbox);
  label = gtk_label_new (_("Download Location:"));
  gtk_container_add (GTK_CONTAINER (hbox), label);
  gtk_widget_show (label);

  window->loclabel = gnome_file_entry_new ("LocationHistory",
					   _("Download Location"));
  gnome_file_entry_set_directory_entry (GNOME_FILE_ENTRY (window->loclabel),
					TRUE);
  gnome_file_entry_set_default_path (GNOME_FILE_ENTRY (window->loclabel),
				     window->prefs->downloads_dir);
  label = gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (window->loclabel));
  gtk_entry_set_text (GTK_ENTRY (label), window->prefs->downloads_dir);
  gtk_container_add (GTK_CONTAINER (hbox), window->loclabel);
  g_signal_connect (G_OBJECT (label), "changed",
		    G_CALLBACK (ed_capplet_set_download_dir), window);
  gtk_widget_show (window->loclabel);
  gtk_widget_show (hbox);
  
  window->noaskdl =
    gtk_radio_button_new_with_label (ask_group,
				     _("Always use default directory"));
  ask_group = gtk_radio_button_group (GTK_RADIO_BUTTON (window->noaskdl));
  gtk_container_add (GTK_CONTAINER (page), window->noaskdl);
  if (window->prefs->always_use_app || window->prefs->always_ask_dir) {
    GTK_TOGGLE_BUTTON (window->noaskdl)->active = FALSE;
  }    
  gtk_widget_show (window->noaskdl);
  g_signal_connect (G_OBJECT (window->noaskdl), "clicked",
		    G_CALLBACK (ed_capplet_set_download_type), window);

  window->usehelper =
    gtk_radio_button_new_with_label (ask_group,
				     _("Always match mime type of file"));
  ask_group = gtk_radio_button_group (GTK_RADIO_BUTTON (window->usehelper));
  gtk_container_add (GTK_CONTAINER (page), window->usehelper);
  if (window->prefs->always_use_dir || window->prefs->always_ask_dir) {
    GTK_TOGGLE_BUTTON (window->usehelper)->active = FALSE;
  }    
  gtk_widget_show (window->usehelper);
  g_signal_connect (G_OBJECT (window->usehelper), "clicked",
		    G_CALLBACK (ed_capplet_set_download_type), window);

  window->askdown =
    gtk_radio_button_new_with_label (ask_group,
				     _("Always ask for a directory"));
  ask_group = gtk_radio_button_group (GTK_RADIO_BUTTON (window->askdown));
  gtk_container_add (GTK_CONTAINER (page), window->askdown);
  if (window->prefs->always_use_app || window->prefs->always_use_dir) {
    GTK_TOGGLE_BUTTON (window->askdown)->active = FALSE;
  }    
  gtk_widget_show (window->askdown);
  g_signal_connect (G_OBJECT (window->askdown), "clicked",
		    G_CALLBACK (ed_capplet_set_download_type), window);

  gtk_widget_show (page);

  page = gtk_vbox_new (FALSE, 0);
  gtk_notebook_append_page (GTK_NOTEBOOK (window->ntbook), page,
			    gtk_label_new (_("General")));

  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (page), hbox);
  label = gtk_label_new (_("Maximum Concurrent Downloads:"));
  gtk_container_add (GTK_CONTAINER (hbox), label);
  gtk_widget_show (label);
  spinadj = gtk_adjustment_new (2, 1, 8, 1, 1, 1);
  window->maxspin = gtk_spin_button_new (GTK_ADJUSTMENT (spinadj), 1, 0);
  gtk_container_add (GTK_CONTAINER (hbox), window->maxspin);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (window->maxspin),
			     window->prefs->max_downloads);
  gtk_signal_connect (GTK_OBJECT (window->maxspin), "changed",
		      G_CALLBACK (ed_capplet_set_max_downloads), window);
  gtk_widget_show (window->maxspin);
  gtk_widget_show (hbox);

  window->donedialog =
    gtk_check_button_new_with_label (_("Display File Complete Dialog"));
  gtk_container_add (GTK_CONTAINER (page), window->donedialog);
  GTK_TOGGLE_BUTTON (window->donedialog)->active = window->prefs->donedialog;
  gtk_signal_connect (GTK_OBJECT (window->donedialog), "clicked",
		      G_CALLBACK (ed_capplet_set_show_dialog), window);
  gtk_widget_show (window->donedialog);

  /*
  window->usedocklet =
    gtk_check_button_new_with_label (_("Use the Status Dock"));
  gtk_container_add (GTK_CONTAINER (page), window->usedocklet);
  GTK_TOGGLE_BUTTON (window->usedocklet)->active = window->prefs->usedocklet;
  gtk_signal_connect (GTK_OBJECT (window->usedocklet), "clicked",
		      GTK_SIGNAL_FUNC (ed_prop_changed), window);
  gtk_widget_show (window->usedocklet);
  */

  gtk_widget_show (page);

  ed_download_type_set_toggle (window);
  g_signal_connect (G_OBJECT (window->window), "destroy",
		    G_CALLBACK (ed_capplet_close), window);
  g_signal_connect (G_OBJECT (window->window), "clicked",
		    G_CALLBACK (ed_capplet_close_clicked), window);

  gtk_widget_show (window->window);
}

gint main (gint argc, gchar *argv[]) {
  GnomeProgram * proggie;

  bindtextdomain(GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
  textdomain(GETTEXT_PACKAGE);

  proggie = gnome_program_init (PACKAGE, VERSION, LIBGNOMEUI_MODULE,
				argc, argv, GNOME_PARAM_POPT_TABLE,
				NULL, NULL);

  ed_capplet_create_window ();
  gtk_main ();
  return 0;
}
