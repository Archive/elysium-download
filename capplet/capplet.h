/*
  Copyright 2001-2002 (C) Elysium GNU/Linux
*/

#ifndef _ED_CAPPLET_H_
#define _ED_CAPPLET_H_

#include <bonobo-activation/bonobo-activation.h>
#include <config.h>
#include "../src/elysium-download.h"
#include "../src/elysium-prefs.h"

typedef struct {
  GtkWidget * window;
  GtkWidget * ntbook;

  GtkWidget * loclabel;
  GtkWidget * noaskdl;
  GtkWidget * usehelper;
  GtkWidget * askdown;

  GtkWidget * maxspin;
  GtkWidget * donedialog;
  GtkWidget * usedocklet;

  GConfClient * client;
  EDPrefs * prefs;
} EDCappletWindow;

#endif
